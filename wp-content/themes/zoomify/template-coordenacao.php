<?php /* Template Name: Coordenação */ ?>
<?php
/**
 * The template for displaying standard pages with sidebar.
 *
 * @package Zoomify
 */

get_header(); ?>

	<div class="tr-container">

	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

        <ul class="coordination-info">        
        <?php $rowCount = count( get_field('periodos') ); //GET THE COUNT ?>
	    <?php $i = 1; ?>
        <?php while(have_rows("periodos")):the_row(); ?>
            <li class="coordination">

                <h2 class="feature">
                    <?php the_sub_field('tempo'); ?>
                </h2>
                
                <?php if(have_rows('coordenadores')): ?>
                <?php while(have_rows('coordenadores')):the_row(); ?>
                <div class="coordenator">
                    <div class="nome">
                        <strong><?php the_sub_field('nome') ?></strong>
                    </div>
                    <?php if(get_sub_field('funcao')):?>
                    <div>
                        (<?php the_sub_field('funcao') ?>)
                    </div>
                    <?php endif; ?>
                    <?php if(get_sub_field('info')):?>
                    <div>
                        <a href="<?php the_sub_field('info'); ?>">
                            <span style="text-decoration:underline;"><strong>Informações</strong></span>
                        </a>
                    </div>
                    <?php endif; ?>
                    <?php if(get_sub_field('contato')):?>
                    <div>
                        <a href="mailto:<?php the_sub_field('contato'); ?>">
                            <span style="text-decoration:underline;"><strong>Contato</strong></span>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
                <?php endif;?>

            </li>
            <?php if($i < $rowCount): ?>
			    <hr>
            <?php endif; ?>
            <?php $i++; ?>
        <?php endwhile; ?>
        </ul>
        
        
        

        


		<?php #comments_template( '', true ); ?>

	<?php endwhile; // end of the loop. ?>

	</div><!-- end .tr-container -->

<?php get_footer(); ?>