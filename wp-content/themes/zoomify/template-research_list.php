<?php /* Template Name: Lista de Grupos */ ?>
<?php
/**
 * The template for displaying standard pages with sidebar.
 *
 * @package Zoomify
 */

get_header(); ?>

	<div class="tr-container">

	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

        <ul class="research-info">        
        <?php $rowCount = count( get_field('grupo_de_pesquisa') ); //GET THE COUNT ?>
	    <?php $i = 1; ?>
        <?php while(have_rows("grupo_de_pesquisa")):the_row(); ?>
            <li class="research">
                <h2 class="feature">
                    <?php the_sub_field('titulo'); ?>
                </h2>
                
                <?php if(get_sub_field('site')): ?>
                <div>
                    <b>Site:</b> 
                    <a href="<?php the_sub_field('site') ?>">
                        <span style="text-decoration:underline;"><?php the_sub_field('site') ?></span>
                    </a>
                </div>
                <?php endif;?>
                
                <?php if(have_rows('contato')): ?>
                <div>
                    <strong>Contato:</strong> 
                    <?php while(have_rows('contato')):the_row(); ?>
                    <span class="contact-info"><?php the_sub_field('nome') ?> - <?php the_sub_field('email'); ?></span>
                    <br />
                    <?php endwhile;?>
                </div>
                <?php endif;?>
                
                <?php if(get_sub_field('cnpq')): ?>
                <div>
                    <strong>Grupo no CNPq: </strong>
                    <a href="<?php the_sub_field('cnpq') ?>"><?php the_sub_field('cnpq') ?></a>
                </div>
                <?php endif;?>
                
                <?php if(get_sub_field('temas')): ?>
                <div>
                    <a href="<?php the_sub_field('temas') ?>">
                        <span style="text-decoration:underline;"><strong>Temas de Pesquisa</strong></span>
                    </a>    
                </div>
                <?php endif;?>
            </li>
            <?php if($i < $rowCount): ?>
			    <hr>
            <?php endif; ?>
            <?php $i++; ?>
        <?php endwhile; ?>
        </ul>
        
        
        

        


		<?php #comments_template( '', true ); ?>

	<?php endwhile; // end of the loop. ?>

	</div><!-- end .tr-container -->

<?php get_footer(); ?>