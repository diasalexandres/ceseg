<?php /* Template Name: Lista Temporal */ ?>
<?php
/**
 * The template for displaying standard pages with sidebar.
 *
 * @package Zoomify
 */

get_header(); ?>

	<div class="tr-container">

	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

        <ul class="comite-info">        
        <?php $rowCount = count( get_field('comite') ); //GET THE COUNT ?>
	    <?php $i = 1; ?>
        <?php while(have_rows("comite")):the_row(); ?>
            <li class="comite">

                <h2 class="feature">
                    <?php the_sub_field('tempo'); ?>
                </h2>
                
                <?php if(have_rows('lista')): ?>
                <ul>
                <?php while(have_rows('lista')):the_row(); ?>
                <li>
                    <?php the_sub_field('item'); ?>
                </li>
                <?php endwhile; ?>
                </ul>
                <?php endif;?>

            </li>
            <?php if($i < $rowCount): ?>
			    <hr>
            <?php endif; ?>
            <?php $i++; ?>
        <?php endwhile; ?>
        </ul>
        
        
        

        


		<?php #comments_template( '', true ); ?>

	<?php endwhile; // end of the loop. ?>

	</div><!-- end .tr-container -->

<?php get_footer(); ?>