<?php /* Template Name: Lista de informações */ ?>
<?php
/**
 * The template for displaying standard pages with sidebar.
 *
 * @package Zoomify
 */

get_header(); ?>

	<div class="tr-container">

	<?php /* Start the Loop */ ?>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

        <ul class="desc-infos">

        <?php while(have_rows("lista")):the_row(); ?>

            <li class="desc-info">

                <h2 class="feature">
                    <?php the_sub_field('titulo'); ?>
                </h2>
                
                <?php the_sub_field('descricao') ?>

            </li>

        <?php endwhile; ?>

        </ul>
        
        
        

        


		<?php #comments_template( '', true ); ?>

	<?php endwhile; // end of the loop. ?>

	</div><!-- end .tr-container -->

<?php get_footer(); ?>