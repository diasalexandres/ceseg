<?php
/**
 * Zoomify functions and definitions
 *
 * @package Zoomify
 */


/**
 * Initialize all the things.
 */
require get_template_directory() . '/inc/init.php';
//acf
require_once get_template_directory() . '/acf/coordenacao.php';
require_once get_template_directory() . '/acf/lista.php';
require_once get_template_directory() . '/acf/texto_lista.php';
require_once get_template_directory() . '/acf/research.php';
/**
 * Note: Do not add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * http://codex.wordpress.org/Child_Themes
 */